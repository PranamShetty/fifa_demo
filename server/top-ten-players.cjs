function topTenPlayers(players) {
  let topTenPlayers = {};
  let matchCount = 0;

  const playerGoals = players.reduce((accumulator, player) => {
    let noOfGoals = player.Event.split("");

    let goal = noOfGoals.reduce((acc, curr) => {
      if (curr === "G") {
        return acc + 1;
      }
      return acc;
    }, 0);

    if (goal > 0) {
      if (accumulator[player["Player Name"]]) {
        accumulator[player["Player Name"]] =
          accumulator[player["Player Name"]] + goal;
      } else accumulator[player["Player Name"]] = goal;
    }
    goal = 0;

    return accumulator;
  }, {});

  const noOfMatches = players.reduce((accumulator, player) => {
    if (accumulator[player["Player Name"]]) {
      accumulator[player["Player Name"]]++;
    } else {
      accumulator[player["Player Name"]] = 1;
    }
    return accumulator;
  }, {});

  let playerStats = Object.keys(playerGoals).reduce((stats, player) => {
    let goals = playerGoals[player];
    let matches = noOfMatches[player];
    let averageGoals = goals / matches;
    stats[player] = averageGoals;
    return stats;
  }, {});

  var keyValueArr = Object.entries(playerStats);

  keyValueArr.sort(function (a, b) {
    return b[1] - a[1];
  });

  for (var i = 0; i < 10; i++) {
    topTenPlayers[keyValueArr[i][0]] = keyValueArr[i][1];
  }

  console.log(topTenPlayers)
  return topTenPlayers;
}

module.exports = topTenPlayers;
