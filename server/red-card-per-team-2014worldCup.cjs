function redCardByTeam(matches, players,year) {
  const YearMatchID = matches.reduce((accumulator, match) => {
    if (match["Year"] == year) {
      accumulator.push(match["MatchID"]);
    }

    return accumulator;
  }, []);

  const teamRedCard = players.reduce((accumulator, player) => {
    if (YearMatchID.includes(player.MatchID) && player["Event"].includes("R")) {
      if (accumulator[player["Team Initials"]]) {
        accumulator[player["Team Initials"]]++;
      } else {
        accumulator[player["Team Initials"]] = 1;
      }
    }
    return accumulator;
  }, {});

  return teamRedCard;
}

module.exports = redCardByTeam;
